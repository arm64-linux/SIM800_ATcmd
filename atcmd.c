#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <sys/ioctl.h>
#include <sys/select.h>


#include "atcmd.h"
#include "pdu.h"
#include "type.h"
#include "config.h"

#define BUFSIZE (1024*20)

static int serial_fd = -1;
static struct termios term;
//static struct termios gOriginalTTYAttrs;
static char *readbuf = NULL;

/*
 * 'open_port()' - Open serial port 1
 * Returns the file descriptor on success or -1 on error.
 */

static int init_port( uint br )
{
    int fd; /* File descriptor for the port */
    int error;

    fd = open( "/dev/ttyUSB0", O_RDWR | O_NOCTTY );
    if ( fd == -1 )
    {
        perror( "open_port: Unable to open /dev/ttyS0 -" );
        return -1;
    }

    //ioctl(fd, TIOCEXCL);
    //设置串口为阻塞
    error = fcntl( fd, F_SETFL, 0 );
    if ( error < 0 )
    {
        perror( "fcntl:" );
        return -1;
    }

    error = tcgetattr( fd, &term );
    if ( error < 0 )
    {
        perror( "tcgetattr:" );
        return -1;
    }
    //gOriginalTTYAttrs = term;

    cfmakeraw( &term );
    cfsetspeed( &term, br );


    term.c_cflag &= ~CSIZE; /* Mask the character size bits */
    term.c_cflag |= CS8;    /* Select 8 data bits */

    //No parity (8N1):
    term.c_cflag &= ~PARENB;
    term.c_cflag &= ~CSTOPB;
    term.c_cflag &= ~CSIZE;
    term.c_cflag |= CS8;
    /**
    CLOCAL:Local line - do not change "owner" of port
    CREAD:Enable receiver
    **/
    term.c_cflag |= CLOCAL | CREAD;

    term.c_iflag = 0;
    term.c_oflag = 0;
    //term.c_lflag = 0;

    term.c_lflag &= ~( ICANON | ECHO | ECHOE | ISIG );  //orignal input

    term.c_cc[VMIN] = 0;    //最少读取字符个数
    term.c_cc[VTIME] = 0;

    error = tcsetattr( fd, TCSANOW, &term );
    if ( error < 0 )
    {
        perror( "cfmakeraw:" );
        return -1;
    }

    return fd;
}

static int sendCmd( int fd, void *buf, size_t size )
{
    int len;

    if ( size < 100 )
    {
        printf( "cmd:%s\n", ( char * )buf );
    }

    if ( ( len = write( fd, buf, size ) ) == -1 )
    {
        perror( "sendCmd" );
        return -1;
    }

    return len;
}

static int readResp( int fd )
{
    int len = 0;
    struct timeval timeout;
    int nfds = fd + 1;
    fd_set readfds;
    int select_ret;

    FD_ZERO( &readfds );
    FD_SET( fd, &readfds );

    // Wait a second
    timeout.tv_sec = 1;
    timeout.tv_usec = 50000;

    while ( ( select_ret = select( nfds, &readfds, NULL, NULL, &timeout ) ) > 0 )
    {
        len += read( fd, readbuf + len, BUFSIZE - len );
        FD_ZERO( &readfds );
        FD_SET( fd, &readfds );
        timeout.tv_sec = 0;
        timeout.tv_usec = 50000;
    }
    printf( "%s\n", readbuf );
    readbuf[len] = 0;
    return len;
}

static void sendAt( int fd )
{
    char cmd[5];
    sprintf( cmd, AT_HEADER );
    sendCmd( fd, cmd, strlen( cmd ) );
}

static void at( int fd )
{
    sendAt( fd );
    for ( ;; )
    {
        if ( readResp( fd ) != 0 )
        {
            if ( strstr( ( const char * )readbuf, "OK" ) != NULL )
            {
                break;
            }
        }
        sendAt( fd );
    }
}

static int sim800l_send_cmd( int fd, char *buf, int len, char *rets, char **revb )
{
    int retlen = 0;

    if ( buf == NULL || rets == NULL || fd < 0 || len <= 0 )
    {
        return -1;
    }

    retlen = sendCmd( fd, buf, len );
    if ( retlen < 0 )
    {
        return -1;
    }

    while ( 1 )
    {
        if ( readResp( serial_fd ) != 0 )
        {
            if ( revb != NULL )
            {
                *revb = readbuf;
            }
            if ( strstr( ( const char * )readbuf, rets ) != NULL )
            {
                break;
            }
            else
            {
                return -2;
            }
        }
    }

    return 0;
}

/*****************************************************************************
 Prototype    : sim800l_save_setting
 Description  : save setting
 Input        : void
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2015/4/23
    Author       : thomas
    Modification : Created function

*****************************************************************************/
int sim800l_save_setting( void )
{
    int err = -1;

    // save all setting
    err = sim800l_send_cmd( serial_fd, CMD_SAVE, sizeof( CMD_SAVE ), CMD_OK, NULL );

    return err;
}
/*****************************************************************************
 Prototype    : sim800l_disable_echo
 Description  : disable command echo
 Input        : void
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2015/4/23
    Author       : thomas
    Modification : Created function

*****************************************************************************/
int sim800l_disable_echo( void )
{
    int err = -1;

    // disable echo,
    err = sim800l_send_cmd( serial_fd, DISABLE_ECHO, sizeof( DISABLE_ECHO ), CMD_OK, NULL );

    return err;
}

/*****************************************************************************
 Prototype    : sim800l_serial_init
 Description  : buadrate auto adjust and set a fixed buadrate
 Input        : uint br
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2015/4/23
    Author       : thomas
    Modification : Created function

*****************************************************************************/
int sim800l_serial_init( uint br )
{
    unsigned int len;
    int err = -1;

    serial_fd = init_port( br );
    if ( serial_fd < 0 )
    {
        perror( "open serial port error\n" );
        return -1;
    }
    readbuf = ( char * )malloc( BUFSIZE );
    printf("------------------>malloc:%p\n",readbuf);
    if ( readbuf == NULL )
    {
        perror( "malloc" );
        return -1;
    }
    memset( readbuf, 0x0, BUFSIZE );

    //buad rate adapt
    at( serial_fd );

    //disable echo & save setting
    if ( ( err = sim800l_disable_echo() ) < 0 )
    {
        printf( "disable echo error,err:%d\n", err );
        return -1;
    }

    err = sim800l_save_setting();
    return err;
}

int sim800l_dial_number( char *number )
{
#if 0
    int i = 0;
#endif
    int len;
    char buf[48];
    
    if ( number == NULL )
    {
        return -1;
    }
#if 0
    len = strlen(number);
    for ( i = 0 ; i < len ; i++ )
    {
        ;
    }
#endif
    len = sprintf(buf,"%s%s;\r", CMD_DAIL_CALL, number);
    if ( len < 0 )
    {
        return -2;
    }
    
    return sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
}

int sim800l_hangup(void)
{
    return sim800l_send_cmd( serial_fd, CMD_HANGUP, sizeof(CMD_HANGUP), CMD_OK, NULL );
}

int sim800l_send_sms( SMS_FORMAT_ENUM format, char *to, char *msg )
{
    int err = -1, segment, mod, seglen, offset = 0;
    unsigned int len, utf8size, pdulen, cmdlen;
    char buf[30];
    char *cmsg = NULL;
    char *ud = NULL, *tud = NULL;

    int alphabet = 2;
    char pdu[1024] = {0};
    int validity = 0; // (vp+1)*5 min
    int to_type = 2;


    if ( to == NULL || msg == NULL )
    {
        return -1;
    }


    if ( format == EN_TEXT )
    {
        len = strlen( msg );
        mod = len % SMS_ASCII_NUM;
        segment = len / SMS_ASCII_NUM + ( mod > 0 );

        do
        {
            memset( buf, 0x0, sizeof( buf ) );
            if ( segment == 1 )
            {
                seglen = mod;
            }
            else
            {
                seglen = SMS_ASCII_NUM;
            }

            cmsg = ( char * )malloc( seglen + 1 );
            printf("------------------>malloc:%p\n",cmsg);
            if ( cmsg == NULL )
            {
                perror( "malloc failed\n" );
                return -1;
            }
            memset( cmsg, 0x0, seglen + 1 );
            memcpy( cmsg, msg + offset, seglen );
            cmsg[seglen] = 0x1A;   //append send code 0x1A(Ctrl+Z)

            //set sms format text,only ascii
            sim800l_send_cmd( serial_fd, CMD_WR_MSG_FORMAT_TEXT, sizeof( CMD_WR_MSG_FORMAT_TEXT ), CMD_OK, NULL );
            //set sms dest number
            err = sprintf( buf, "%s\"%s\"\r", CMD_SEND_SMS, to );
            err = sim800l_send_cmd( serial_fd, buf, err, CMD_SMS_INPUT, NULL );
            if ( err < 0 )
            {
                if ( cmsg != NULL )
                {
                    printf("------------------>free:%p\n",cmsg);
                    free( cmsg );
                    cmsg = NULL;
                }
                return -1;
            }
            //send message
            sim800l_send_cmd( serial_fd, cmsg, seglen + 1, CMD_OK, NULL );
            if ( err < 0 )
            {
                if ( cmsg != NULL )
                {
                    printf("------------------>free:%p\n",cmsg);
                    free( cmsg );
                    cmsg = NULL;
                }
                return -1;
            }

            offset += seglen;
            
            if ( cmsg != NULL )
            {
                printf("------------------>free:%p\n",cmsg);
                free( cmsg );
                cmsg = NULL;
            }
        }
        while ( --segment );

    }
    else
    {
        //set sms format PDU
        sim800l_send_cmd( serial_fd, CMD_WR_MSG_FORMAT_PDU, sizeof( CMD_WR_MSG_FORMAT_PDU ), CMD_OK, NULL );

        utf8size = GetUTF8Len( msg );
        len = utf8size << 1;    //unicode is 2 byte
        mod = len % SMS_ASCII_NUM;
        segment = len / SMS_ASCII_NUM + ( mod > 0 );

        ud = ( char * )malloc( len * 2 + 1 ); // one byte need two charactors
        printf("------------------>malloc:%p\n",ud);
        if ( ud == NULL )
        {
            perror( "malloc failed\n" );
            return -1;
        }
        tud = ud;
        memset( tud, 0x0, len * 2 + 1 );

        if ( make_ud( msg, utf8size, tud )  == NULL )
        {
            printf( "make ud error\n" );
            return -1;
        }

        do
        {
            if ( segment == 1 )
            {
                seglen = mod;
            }
            else
            {
                seglen = SMS_ASCII_NUM;
            }

            memset( pdu, 0x0, sizeof( pdu ) );
            pdulen = make_pdu( to, to_type, msg, alphabet, validity, pdu,  tud, seglen );
            tud += seglen << 1;

            cmdlen = sprintf( buf, "%s%d\r", CMD_SEND_SMS, pdulen );
            if ( cmdlen < 0 )
            {
                perror( "sprintf" );
            }
            err = sim800l_send_cmd( serial_fd, buf, cmdlen, CMD_SMS_INPUT, NULL );
            if ( err < 0 )
            {
                printf( "send input error\n" );
                if ( ud != NULL )
                {
                    printf("------------------>free:%p\n",ud);
                    free( ud );
                    ud = NULL;
                }
                return -1;
            }
            err = sim800l_send_cmd( serial_fd, pdu, strlen( pdu ), CMD_OK, NULL );
            if ( err < 0 )
            {
                printf( "send ok error\n" );
                if ( ud != NULL )
                {
                    printf("------------------>free:%p\n",ud);
                    free( ud );
                    ud = NULL;
                }
                return -1;
            }
        }
        while ( --segment );

        if ( ud != NULL )
        {
            printf("------------------>free:%p\n",ud);
            free( ud );
            ud = NULL;
        }

    }

    return 0;
}

int sim800l_get_sms( int idx, RECVPDU_STRU *revpdu )
{
    int len, ret, i = 0;
    char *revb = NULL, *seg = NULL;
    char buf[20];
    if ( idx < 0 || revpdu == NULL )
    {
        return -1;
    }

    len = sprintf( buf, "%s%d\r", CMD_REC_SMS_BY_INDEX, idx );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, &revb );
    if ( ret < 0 )
    {
        return -1;
    }

    if ( revb != NULL )
    {
        while ( ( seg = strsep( &revb, "\r\n" ) ) != NULL )
        {
            printf("seg:%s\n",seg);
            i++;
            if ( i == 5 )
            {
                parser_pdu( seg, revpdu );
                break;
            }
        }
    }

    return 0;
}

int sim800l_listen_sms( void )
{
    char *sp = NULL;

    while ( 1 )
    {
        if ( readResp( serial_fd ) > 0 )
        {
            if ( (( sp = strstr( ( const char * )readbuf, CMD_SMS_RECV_MT ) ) != NULL) ||
                  (( sp = strstr( ( const char * )readbuf, CMD_SMS_RECV_ME ) ) != NULL) ||
                  (( sp = strstr( ( const char * )readbuf, CMD_SMS_RECV_SM ) ) != NULL))
            {
                sp = strstr( sp, "," );
                if ( sp != NULL )
                {
                    sp += 1;
                    return atoi( sp );
                }
                break;
            }
        }
    }

    return -1;
}

#if 0
int sim800l_mms_parament_init(MMS_SETTING *pmmssetting)
{
    int ret, len;
    char buf[100];

    if ( pmmssetting == NULL )
    {
        return -2;   
    }
    
    //enter mms mode
    ret = sim800l_send_cmd( serial_fd, CMD_MMS_INIT, sizeof( CMD_MMS_INIT ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto init_err;
    }
    
    //set mms url
    len = sprintf( buf, "%s\"%s\"\r", CMD_MMS_URL, pmmssetting->mmsc );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto init_err;
    }
    //set mms content id
    len = sprintf( buf, "%s%d\r", CMD_MMS_CID, 1 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto init_err;
    }
    //set mms vpn and port
    len = sprintf( buf, "%s\"%s\",%d\r", CMD_MMS_PROTO, pmmssetting->vpnip, pmmssetting->port );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto init_err;
    }
    //set mms cfg
    len = sprintf( buf, "%s%d,%d,%d,%d,%d,%d,%d,%d\r", CMD_MMS_SENDCFG, 6, 3, 0, 0, 2, 4, 1, 0 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto init_err;
    }
    

init_err:
    ret = sim800l_send_cmd( serial_fd, CMD_MMS_TERM, sizeof( CMD_MMS_TERM ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        return ret;
    }
    return 0;
}
#endif

int sim800l_send_mms(MMS_SETTING *pmmssetting, char *file, char *discribe, char *numlist[], int num )
{
    int i, ret, len, fd = -1;
    char buf[100];
    char *pbuf = NULL;
    struct stat st;
    char bearer_opened = 0;
    
    if ( num == 0 || numlist == NULL || file == NULL || discribe == NULL )
    {
        return -1;
    }

    ret = stat( file, &st );
    if ( ret < 0 )
    {
        perror( "stat" );
        return ret;
    }

    fd = open( file, O_RDWR );
    if ( fd < 0 )
    {
        perror( "open" );
        return fd;
    }

    //enter mms mode
    ret = sim800l_send_cmd( serial_fd, CMD_MMS_INIT, sizeof( CMD_MMS_INIT ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        close( fd );
        return ret;
    }

    pbuf = ( char * )malloc( st.st_size + 2 );
    printf("------------------>malloc:%p\n",pbuf);
    if ( pbuf ==  NULL )
    {
        perror( "malloc" );
        goto exit;
    }
    memset( pbuf, 0x0, st.st_size + 2 );
    // little endian
    pbuf[0] = 0xFF;
    pbuf[1] = 0xFE;
    ret = read( fd, &pbuf[2], st.st_size );
    if ( ret != st.st_size )
    {
        perror( "read" );
        goto exit;
    }
    
    //set mms url
    len = sprintf( buf, "%s\"%s\"\r", CMD_MMS_URL, pmmssetting->mmsc );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    //set mms content id
    len = sprintf( buf, "%s%d\r", CMD_MMS_CID, 1 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    //set mms vpn and port
    len = sprintf( buf, "%s\"%s\",%d\r", CMD_MMS_PROTO, pmmssetting->vpnip, pmmssetting->port );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    //set mms cfg
    len = sprintf( buf, "%s%d,%d,%d,%d,%d,%d,%d,%d\r", CMD_MMS_SENDCFG, 6, 3, 0, 0, 2, 4, 1, 0 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    
    //AT+SAPBR=3,1,"Contype","GPRS"
    len = sprintf( buf, "%s%d,%d,%s,%s\r", CMD_MMS_SAPBR, 3, 1, "\"Contype\"", "\"GPRS\"" );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }

    //AT+SAPBR=3,1,"APN","CMWAP"
    len = sprintf( buf, "%s%d,%d,%s,%s%s%s\r", CMD_MMS_SAPBR, 3, 1, "\"APN\"", "\"", pmmssetting->net, "\"" );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }


    //AT+SAPBR=1,1  //open
    len = sprintf( buf, "%s%d,%d\r", CMD_MMS_SAPBR, 1, 1 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    bearer_opened = 1;
    
    #if 0
    //AT+SAPBR=2,1
    len = sprintf( buf, "%s%d,%d\r", CMD_MMS_SAPBR, 2, 1 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_SAPBR, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    #endif
    
    //enter mms edit mode
    len = sprintf( buf, "%s%d\r", CMD_MMS_EDIT, 1 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }

    //download mms payload
    //AT+CMMSDOWN="TITLE",12963,20000
    len = sprintf( buf, "%s\"%s\",%d,%d\r", CMD_MMS_DOWN, "TITLE", strlen(discribe), 20000 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_CONNECT, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }

    //download title text
    ret = sim800l_send_cmd( serial_fd, discribe, strlen(discribe), CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    
    //AT+CMMSDOWN="PIC",12963,20000
    len = sprintf( buf, "%s\"%s\",%d,%d\r", CMD_MMS_DOWN, "PIC", ( int )( st.st_size ), 20000 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_CONNECT, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }

    //download pic
    ret = sim800l_send_cmd( serial_fd, pbuf, st.st_size + 2, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }


    for ( i = 0; i < num; i++ )
    {
        if ( numlist[i] != NULL )
        {
            if ( i == 0 )
            {
                //add first reviever number
                len = sprintf( buf, "%s\"%s\"\r", CMD_MMS_RECP, numlist[i] );
                ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
                if ( ret < 0 )
                {
                    goto exit;
                }
            }
            else
            {
                len = sprintf( buf, "%s\"%s\"\r", CMD_MMS_CC, numlist[i] );
                ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
                if ( ret < 0 )
                {
                    goto exit;
                }
            }
        }
    }

    //add mms email
    #if 0
    len = sprintf( buf, "%s%s\r", CMD_MMS_BCC, "tang_m_cong@163.com" );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }
    #endif
    
    //send mms
    ret = sim800l_send_cmd( serial_fd, CMD_MMS_SEND, sizeof( CMD_MMS_SEND ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }

    //exit mms edit mode
    len = sprintf( buf, "%s%d\r", CMD_MMS_EDIT, 0 );
    ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    if ( ret < 0 )
    {
        goto exit;
    }

exit:
    //exit mms mode
    if ( fd > 0 )
    {
        close( fd );
    }

    if ( pbuf != NULL )
    {
    
        printf("------------------>free:%p\n",pbuf);
        free( pbuf );
        pbuf = NULL;
    }
    
    if ( bearer_opened )
    {
        //close bearer
        len = sprintf( buf, "%s%d,%d\r", CMD_MMS_SAPBR, 0, 1 );
        ret = sim800l_send_cmd( serial_fd, buf, len, CMD_OK, NULL );
    }
    
    ret = sim800l_send_cmd( serial_fd, CMD_MMS_TERM, sizeof( CMD_MMS_TERM ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        return ret;
    }

    return ret;
}


int sim800l_send_email(void)
{
    int ret;

    /* 设置承载场景 */
    ret = sim800l_send_cmd( serial_fd, CMD_EMAIL_PAYLOAD1, sizeof( CMD_EMAIL_PAYLOAD1 ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        return ret;
    }

    ret = sim800l_send_cmd( serial_fd, CMD_EMAIL_PAYLOAD2, sizeof( CMD_EMAIL_PAYLOAD2 ), CMD_OK, NULL );
    if ( ret < 0 )
    {
        return ret;
    }
}

/*****************************************************************************
 Prototype    : sim800l_close
 Description  : close serial port fd
 Input        : void
 Output       : None
 Return Value :
 Calls        :
 Called By    :

  History        :
  1.Date         : 2015/4/24
    Author       : thomas
    Modification : Created function

*****************************************************************************/
void sim800l_close( void )
{
    close( serial_fd );

    if ( readbuf != NULL )
    {
        printf("------------------>free:%p\n",readbuf);
        free( readbuf );
        readbuf = NULL;
    }
}

