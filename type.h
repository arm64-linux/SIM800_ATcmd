#ifndef __TYPE_H__
#define __TYPE_H__


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

typedef  unsigned char uchar;
typedef  unsigned short ushort;
typedef  unsigned int uint;

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __TYPE_H__ */

