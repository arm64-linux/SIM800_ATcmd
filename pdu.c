/*
SMS Server Tools 3
Copyright (C) 2006- Keijo Kasvi
http://smstools3.kekekasvi.com/

Based on SMS Server Tools 2 from Stefan Frings
http://www.meinemullemaus.de/
SMS Server Tools version 2 and below are Copyright (C) Stefan Frings.

This program is free software unless you got it under another license directly
from the author. You can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation.
Either version 2 of the License, or (at your option) any later version.
*/

#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include <syslog.h>
#include "pdu.h"
#include "smsd_cfg.h"
#include "charset.h" // required for conversions of partial text content.
#include "config.h"

/* Swap every second character */
void swapchars(char* string) 
{
  int length;
  int position;
  char c;

  length = strlen(string);
  for (position = 0; position < length - 1; position += 2)
  {
    c = string[position];
    string[position] = string[position+1];
    string[position + 1] = c;
  }
}


/* Converts binary to PDU string, this is basically a hex dump. */
void binary2pdu(ushort* binary, int length, char* pdu)
{
  int character;
  char octett[5];

  //if (length>maxsms_binary)
    //length=maxsms_binary;
  pdu[0]=0;
  for (character=0;character<length; character++)
  {
    sprintf(octett,"%04X",(ushort) binary[character]);
    strcat(pdu,octett);
  }
}

char* make_ud(char *msg, int utf8size, char* buf)
{
    ushort *tmp2 = NULL;
    
    if ( msg == NULL || buf == NULL )
    {
        return NULL;
    }

    tmp2 = (ushort *)malloc( (utf8size+1)*2 );
    printf("------------------>malloc:%p\n",tmp2);
    if ( tmp2 == NULL )
    {
        perror("malloc:");
        return NULL;
    }
    memset(tmp2, 0x0, (utf8size+1)*2);
    
    if ( NULL == UTF82uni(tmp2, msg) )
    {
        printf("UTF82uni error\n");
        if ( tmp2 != NULL )
        {
            printf("------------------>free:%p\n",tmp2);
            free(tmp2);
            tmp2= NULL;
        }
        return NULL;
    }

    binary2pdu(tmp2, utf8size, buf);
    if ( tmp2 != NULL )
    {
        printf("------------------>free:%p\n",tmp2);
        free(tmp2);
        tmp2= NULL;
    }

    return buf;
}

/****************************************************************************************************************************************
1. if SMSC address length is equals 0,pdu format is: 00+TPDU,SMSC address information only with length:00.
2. if SMSC address length is not equals 0, format is: SMSC address information + TPDU, 
    SMSC address information is contained:
        SMSC address info length + SMSC address type + SMSC address
        SMSC address info length is total octet of SMSC address type & address
-----------------------------------------------------------------------------------------------------------------------------------------
[In send mode]

TPDU format:
    @FO(First-Octec<M>)
           -----------------------------------------------------
           | TP-RP | TP-UDHI | TP-SRR | TP-VPF | TP-RD |TP-MTI |
           -----------------------------------------------------
           |   7   |    6    |    5   |   4:3  |    2  |   1:0 |
           -----------------------------------------------------
           in this case  TP-RP,TP-UDHI, TP-SRR,TP-RD, usually set as 0.
           TP-VPF:TP-Validity-Period-Format
                 [00] vp is not exsit
                 [01] reserve
                 [10] vp is exsit,format is relative time
                 [11] vp is exsit,format is absolute time

   @MR(Message Reference<M>)
            1 byte message counter, ranged from 1 to 255,usually set as 0

   @DA (Destination Address<M>)
            like SMSC information

   @PID(Protocol Identifier<M>)
            1 byte, set as 0

   @DCS(Data Coding Scheme<M>)
            1 byte, in this case only care about bit3 bit2,other bit set 0
            [3:2]
                00:7-bit,DCS=0x00
                01:8-bit,DCS=0x40
                10:16bit,DCS=0x80
                11:reserve

    @VP (Valid Period<O>)
            1 byte,this byte is indicate by FO:TP-VPF,how long a message will keeped in sms center before send to destination terminal.
                00－8F  (VP+1)*5min
                90－A7  12H +(VP－143)*30min
                A8－C4  (VP-166)*1day
                C5－FF  (VP-192)*1week

    @UDL (UD Length<M>)

    @UD  (<O>)
****************************************************************************************************************************************/
int make_pdu(char * number, int number_type, char * message, int alphabet, int validity, char * pdu, char *ud, int seglen)
{
    uint len = 0;
    uchar nf, coding;
    char tmp[30];
    uint numberlength;
    uchar fo = 0x11;
    
    if ( number == NULL )
    {
        return -1;
    }
    
    switch (number_type)
    {
      case 0: // "unknown"
        nf = NF_UNKNOWN; // 1000 0001
        break;
    
      case 1: // "international"
        nf = NF_INTERNATIONAL; // 1001 0001
        break;
    
      case 2: // "national"
        nf = NF_NATIONAL; // 1010 0001
        break;
        
      default:
        nf = NF_NATIONAL;
    }

    memset(tmp, 0x0, sizeof(tmp));
    numberlength = strlen(number);
    strcpy(tmp, number);
    // terminate the number with F if the length is odd
    if (numberlength%2)
    {
        strcat(tmp,"F");
    }
    swapchars(tmp);
    
    if (alphabet == 1)
        coding = 4; // 8bit binary
    else if (alphabet == 2)
        coding = 8; // 16bitmake
        
    else
        coding = 0; // 7bit    

    sprintf(pdu, "00%02X00%02X%02X%s00%02X%02X%02X", fo, numberlength, nf, tmp, coding, validity, seglen);
    strncat(pdu, ud, seglen<<1);
    len = strlen(pdu);
    pdu[len] = 0x1A; // append Ctrl+Z

    len = (len -2) / 2; // ignore first byte 0x00,SMSC length
    
    return len;
}

static int IsHexChar( char c)
{
    if ( c >= '0' && c <= '9' )
    {
        return 0;
    }
    else if ( c >= 'a' && c <= 'f' )
    {
        return 1;
    }
    else if ( c >= 'A' && c <= 'F' )
    {
        return 2;
    }
    
    return -1;
}

int pdutxt2binary(const char *pdu, char *bin)
{
    int ret, i = 1;
    unsigned char e, msb, lsb;
    
    if ( pdu == NULL || bin == NULL )
    {
        return -1;
    }

    while ( *pdu )
    {
        ret = IsHexChar(*pdu);
        if ( ret == 0 )
        {
            e = *pdu - '0';
        }
        else if ( ret == 1 )
        {
            e = (*pdu - 'a') + 10;
        }
        else if ( ret == 2 )
        {
            e = (*pdu - 'A') + 10;
        }
        else
        {
            return -1;
        }

        if ( i%2 )
        {
            msb = e<<4;
        }
        else
        {
            lsb  =e;
            *bin = msb + lsb;
            bin++;
        }

        i++;
        pdu++;
    }
    return 0;
}

static int gsmlongmsgconvert(const unsigned char* pSrc, char* pDst, int nSrcLength)
{
    int n = 0;
    unsigned char tmp;

    if ( pSrc == NULL || pDst== NULL)
    {
        return -1;
    }
    
    while ( n < nSrcLength )
    {
        tmp = *(pSrc+n) >> 1;
        if ( n != (nSrcLength - 1) )
        {
           *(pDst+n) = ( (*(pSrc+1+n) & 0x1) << 7) | tmp; 
        }
        else
        {
            *(pDst+n) = tmp;
        }
        n++;
    }
    
    
    return 0;
}

// 7bit解码
// 输入: pSrc - 源编码串指针
//       nSrcLength - 源编码串长度
// 输出: pDst - 目标字符串指针
// 返回: 目标字符串长度
int gsmDecode7bit(const unsigned char* pSrc, char* pDst, int nSrcLength)
{
    int nSrc; // 源字符串的计数值
    int nDst; // 目标解码串的计数值
    int nByte; // 当前正在处理的组内字节的序号，范围是0-6
    unsigned char nLeft; // 上一字节残余的数据

    // 计数值初始化
    nSrc = 0;
    nDst = 0;
    // 组内字节序号和残余数据初始化
    nByte = 0;
    nLeft = 0;

    // 将源数据每7个字节分为一组，解压缩成8个字节
    // 循环该处理过程，直至源数据被处理完
    // 如果分组不到7字节，也能正确处理
    while(nSrc<nSrcLength)
    {
        // 将源字节右边部分与残余数据相加，去掉最高位，得到一个目标解码字节
        *pDst = ((*pSrc << nByte) | nLeft) & 0x7f;

        // 将该字节剩下的左边部分，作为残余数据保存起来
        nLeft = *pSrc >> (7-nByte);

        // 修改目标串的指针和计数值
        pDst++;
        nDst++;

        // 修改字节计数值
        nByte++;

        // 到了一组的最后一个字节
        if(nByte == 7)
        {
            // 额外得到一个目标解码字节
            *pDst = nLeft;

            // 修改目标串的指针和计数值
            pDst++;
            nDst++;

            // 组内字节序号和残余数据初始化
            nByte = 0;
            nLeft = 0;
        }

        // 修改源串的指针和计数值
        pSrc++;
        nSrc++;
    }

    // 输出字符串加个结束符
    *pDst = '\0';

    // 返回目标串长度
    return nDst;
}

int gsmEncode7bit(unsigned char* pDst,const char* pSrc)
{
    int nSrc;        // 源字符串的计数值
    int nDst;        // 目标编码串的计数值
    int nChar;       // 当前正在处理的组内字符字节的序号，范围是0-7
    unsigned char nLeft;    // 上一字节残余的数据
    int nSrcLength=strlen(pSrc);



    // 计数值初始化
    nSrc = 0;
    nDst = 0;

    // 将源串每8个字节分为一组，压缩成7个字节
    // 循环该处理过程，直至源串被处理完
    // 如果分组不到8字节，也能正确处理
    while(nSrc<nSrcLength)
    {
        // 取源字符串的计数值的最低3位
        nChar = nSrc & 7;

        // 处理源串的每个字节
        if(nChar == 0)
        {
            // 组内第一个字节，只是保存起来，待处理下一个字节时使用
            nLeft = *pSrc;
        }
        else
        {
            // 组内其它字节，将其右边部分与残余数据相加，得到一个目标编码字节
            *pDst = (*pSrc << (8-nChar)) + nLeft;

            // 将该字节剩下的左边部分，作为残余数据保存起来
            nLeft = *pSrc >> nChar;
            // 修改目标串的指针和计数值 pDst++;
            //printf("%c",*pDst);
            pDst++;  nDst++;
        }
        // 修改源串的指针和计数值
        pSrc++; nSrc++;
    }

    //Nleft还有剩余，需要一个自己保留。
    nChar = nSrc & 7;
    if(nChar != 0)
    {
        *pDst=nLeft;
        nDst++; 
        pDst++;
    }
    *pDst='\0';

    // 返回目标串长度
    return nDst;
}


int parser_pdu(const char * pdu, RECVPDU_STRU *revsms)
{
    unsigned char *bin = NULL, *ptr = NULL;
    int i;
    int pdulen, numlen, offset;
    
    if ( pdu == NULL || revsms == NULL )
    {
        return -1;
    }

    pdulen = strlen(pdu);
    printf("===============>pdulen:%d\n",pdulen);
    bin = ( char * )malloc(pdulen);
    if  ( bin == NULL )
    {
        return -1;
    }

    memset(bin, 0x0, pdulen);
    pdutxt2binary(pdu, bin);

    offset = 0;
    revsms->sca.addr_length = bin[offset];
    if ( revsms->sca.addr_length != 0 ) 
    {
        offset++;
        revsms->sca.addr_type = bin[offset];
        offset++;
        //get sca addr
        if ( revsms->sca.addr_type == NF_INTERNATIONAL )
        {
            numlen = revsms->sca.addr_length - 2;   // exclude sca type and 86
            offset++;
        }
        else
        {
            numlen = revsms->sca.addr_length - 1;   // exclude sca type
        }
        ptr = revsms->sca.addr_num;
        for (i=0; i < numlen; i++)
        {
            ptr += sprintf(ptr, "%02X", bin[i+offset] );
        }
        swapchars(revsms->sca.addr_num);
        if ( revsms->sca.addr_num[numlen * 2 -1] == 'F' )
        {
            revsms->sca.addr_num[numlen * 2 -1] = 0;
        }
#if PDU_DBG
        printf("revsms->sca.addr_num:%s\n",revsms->sca.addr_num);
#endif
        offset = revsms->sca.addr_length + 1;
        
        //get recv fo
        memcpy(&(revsms->fo.rfo) ,&bin[offset], 1);
#if PDU_DBG
        printf("rforp:%d\n",revsms->fo.rfo.rforp);
        printf("rfoudhi:%d\n",revsms->fo.rfo.rfoudhi);
        printf("rfosri:%d\n",revsms->fo.rfo.rfosri);
        printf("rforeverse:%d\n",revsms->fo.rfo.rforeverse);
        printf("rfomms:%d\n",revsms->fo.rfo.rfomms);
        printf("rfomti:%d\n",revsms->fo.rfo.rfomti);
#endif
        
        //get oa
        offset++;
        revsms->oa.addr_length = bin[offset];
        offset++;
        revsms->oa.addr_type = bin[offset];
        offset++;
        if ( revsms->oa.addr_type == NF_INTERNATIONAL )
        {
            numlen = (revsms->oa.addr_length+1>>1) - 1;   // exclude sca type and 86
            offset ++;
        }
        else
        {
            numlen = revsms->oa.addr_length+1>>1;   // exclude sca type
        }
        ptr = revsms->oa.addr_num;
        for (i=0; i < numlen; i++)
        {
            ptr += sprintf(ptr, "%02X", bin[i+offset] );
        }
        swapchars(revsms->oa.addr_num);
        if ( revsms->oa.addr_num[numlen * 2 -1] == 'F' )
        {
            revsms->oa.addr_num[numlen * 2 -1] = 0;
        }
#if PDU_DBG
        printf("revsms->oa.addr_num:%s\n",revsms->oa.addr_num);
#endif        
        offset += numlen;

        //get pid
        revsms->pid = bin[offset];
        offset++;
        
        //get dcs
        memcpy(&(revsms->dcs) ,&bin[offset], 1);
        offset++;
#if PDU_DBG
        printf("dsc_reverse:%d\n",revsms->dcs.dsc_reverse);
        printf("dsc_compressed:%d\n",revsms->dcs.dsc_compressed);
        printf("dsc_ib1b0:%d\n",revsms->dcs.dsc_ib1b0);
        printf("dsc_coded:%d\n",revsms->dcs.dsc_coded);
        printf("dsc_class:%d\n",revsms->dcs.dsc_class);
#endif

        //get stcs
        revsms->scts.year = SWAP_HALF_BYTE(bin[offset]);
        offset++;
        revsms->scts.month= SWAP_HALF_BYTE(bin[offset]);
        offset++;
        revsms->scts.day= SWAP_HALF_BYTE(bin[offset]);
        offset++;
        revsms->scts.hour= SWAP_HALF_BYTE(bin[offset]);
        offset++;
        revsms->scts.min= SWAP_HALF_BYTE(bin[offset]);
        offset++;
        revsms->scts.sec= SWAP_HALF_BYTE(bin[offset]);
        offset++;
        revsms->scts.timezone= SWAP_HALF_BYTE(bin[offset]);
        offset++;
#if PDU_DBG
        printf("Y:%02X M:%02X D:%02X H:%02X M:%02X S:%02X T:%02X \n",\
        revsms->scts.year,revsms->scts.month,revsms->scts.day,revsms->scts.hour, \
        revsms->scts.min, revsms->scts.sec, revsms->scts.timezone);
#endif
        //get udl
        revsms->udl = bin[offset];
#if PDU_DBG        
        printf("udl:%d\n",revsms->udl);
#endif        
        offset++;

        if ( revsms->fo.rfo.rfoudhi != 0 )  //has udh
        {
            revsms->ud.udh.udhl = bin[offset];
            offset++;
    #if PDU_DBG
            printf("udhl:%d\n",revsms->ud.udh.udhl);
    #endif
            if ( revsms->ud.udh.udhl == 0 )
            {
                #if 0
                offset++;
                revsms->ud.msg.content_len = bin[offset];
                offset++;
                if ( revsms->dcs == 0x08 )
                {
                    uni2UTF8( (unsigned short *)&bin[offset], revsms->ud.msg.msg);
                    printf("UCS2 msg:%s\n",revsms->ud.msg.msg);
                }
                else
                {
                    printf("not finish convert\n");
                }
                #endif
            }
            else
            {
                revsms->ud.udh.iei = bin[offset];
                offset++;
                revsms->ud.udh.iedl = bin[offset];
                offset++;

                switch ( revsms->ud.udh.iei )
                {
                    case 0:
                        memcpy(&revsms->ud.udh.ied.ssms, &bin[offset], revsms->ud.udh.iedl);
                        offset += revsms->ud.udh.iedl;
                        break;
                    case 1:break;
                    case 2:break;
                    case 3:break;
                    case 4:break;
                    case 5:break;
                    case 6:break;
                    case 7:break;
                    default:break;
                }
            }
        }

        if ( revsms->dcs.dsc_coded == EN_UCS2 )
        {
            uni2UTF8( (unsigned short *)&bin[offset], revsms->ud.msg);
            printf("UCS2 msg:%s\n",revsms->ud.msg);
        }
        else if ( revsms->dcs.dsc_coded == EN_7BIT )
        {
            if ( revsms->fo.rfo.rfoudhi != 0 )
            {
                gsmlongmsgconvert(&bin[offset], &bin[offset], revsms->udl - revsms->ud.udh.udhl - 2);
                gsmDecode7bit(&bin[offset], revsms->ud.msg, revsms->udl - revsms->ud.udh.udhl - 2);
            }
            else
            {
                gsmDecode7bit(&bin[offset], revsms->ud.msg, revsms->udl);
            }
            printf("7bit msg:%s\n", revsms->ud.msg);
        }
        else if ( revsms->dcs.dsc_coded == EN_8BIT )
        {
            memcpy(revsms->ud.msg, &bin[offset], revsms->udl);
            printf("8bit msg:%s\n", revsms->ud.msg);
        }
        else
        {
            printf("invalid msg\n");
        }
    }

    
    if ( bin )
    {
        free(bin);
        bin = NULL;
    }
    return 0;
}


