#ifndef __CONFIG_H__
#define __CONFIG_H__

#if 1//def DEBUG
#define  tmc_dbg(format,arg...) do{printf((format),##arg);}while(0)	
#else
#define  tmc_dbg(...)  do{}while(0)
#endif

#endif /* __CONFIG_H__ */
